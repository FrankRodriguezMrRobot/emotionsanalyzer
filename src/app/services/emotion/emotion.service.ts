import "rxjs/add/operator/share";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class EmotionService {
  constructor(private api: ApiService) { }

  getData(){
    const requestOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    let request = this.api.get('797736473940874/comments?fields=message&limit=200&access_token=1050963508435852%7Cr_fh9xcEq1smrfPRLZ9i1-vmSZQ', requestOptions).share();
    request.subscribe(res => {
      console.info('1 SUCCESSFULL', res);
    }, err => {
      console.error('ERROR', err);
    });
    return request;
  }
}