import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Global from '../../../../globals';
/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class ApiService {
  private baseUrl = Global.facebook_url;
  constructor(public http: HttpClient) {
  }

  get(endpoint: string, reqOpts?: any) {
    return this.http.get(this.baseUrl+ '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(this.baseUrl + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.baseUrl + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.baseUrl + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.baseUrl + '/' + endpoint, body, reqOpts);
  }

  getWithoutPath(endpoint: string, reqOpts?: any) {
    return this.http.get(endpoint, reqOpts);
  }
}