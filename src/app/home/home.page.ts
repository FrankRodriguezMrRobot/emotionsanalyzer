import { EmotionService } from '../services/emotion/emotion.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  urlAddress;
  commentsFromFacebook;
  numberTotalComments = 0;
  items=[
    {src:"../../assets/angry.png",text:"Enojado"},
    {src:"../../assets/bored.png",text:"Aburrido"},
    {src:"../../assets/excited.png",text:"Emocionado"},
    {src:"../../assets/fear.jpg",text:"Temor"},
    {src:"../../assets/happy.png",text:"Feliz"},
    {src:"../../assets/sad.png",text:"Triste"}
  ];
  constructor(public emotionService:EmotionService){
    //this.emotionService.getData();
  }
  getCommentId(){
    this.emotionService.getData().subscribe(res=>{
      this.commentsFromFacebook=res['data'];
      this.numberTotalComments=this.commentsFromFacebook.length;
    });
    //console.warn(this.urlAddress);
  }
}
